from keras.models import Sequential
from keras.layers import Dense
from sklearn import metrics
from sklearn.model_selection import KFold
import numpy as np
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import SVC

class Tester():
    def test_on_data(self, x, y, k=2):
        score_sum = 0
        kf = KFold(n_splits=k)
        kf.get_n_splits(x)
        for i in range(5):
            for train_index, test_index in kf.split(x):
                x_train, x_test = x[train_index], x[test_index]
                y_train, y_test = y[train_index], y[test_index]

                model = Sequential()
                model.add(Dense(units=10, activation='relu', input_dim=len(x[0])))
                #model.add(Dense(units=10, activation='softmax'))
                model.add(Dense(units=20, activation='sigmoid'))
                #model.add(Dense(units=50, activation='sigmoid'))
                model.add(Dense(units=1, activation='sigmoid'))

                model.compile(loss='binary_crossentropy', optimizer='sgd', metrics=['accuracy'])
                model.fit(x_train, y_train, epochs=50, batch_size=32, verbose=0)

                scores = model.evaluate(x_test, y_test, batch_size=32)
                score_sum += scores[1]*100

                del model
        return np.round(score_sum / (k*5), 2)

    def test_on_data_naive_bayes(self, x, y, k=2, epochs=5, batch_size=32, verbose=0):
        score_sum = 0
        kf = KFold(n_splits=k)
        kf.get_n_splits(x)
        for i in range(5):
            for train_index, test_index in kf.split(x):
                x_train, x_test = x[train_index], x[test_index]
                y_train, y_test = y[train_index], y[test_index]

                model = GaussianNB()
                model.fit(x_train, y_train.ravel())

                predicted = model.predict(x_test)
                correct = 0
                for i in range(len(predicted)):
                    if (predicted[i] == y_test[i]):
                        correct += 1

                #print(metrics.classification_report(y_test, predicted))
                # print("NB accuracy: {}".format((correct / len(y_test)*100)))
                score_sum += (correct / len(y_test) * 100)
                #
                del model
        return np.round(score_sum / (k*5), 2)


    def test_on_data_svm(self, x, y, k=2, epochs=5, batch_size=32, verbose=0):
        score_sum = 0
        kf = KFold(n_splits=k)
        kf.get_n_splits(x)
        for i in range(5):
            for train_index, test_index in kf.split(x):
                x_train, x_test = x[train_index], x[test_index]
                y_train, y_test = y[train_index], y[test_index]

                model = SVC()
                model.fit(x_train, y_train.ravel())

                predicted = model.predict(x_test)
                correct = 0
                for i in range(len(predicted)):
                    if (predicted[i] == y_test[i]):
                        correct += 1

                #print(metrics.classification_report(y_test, predicted))
                # print("NB accuracy: {}".format((correct / len(y_test)*100)))
                score_sum += (correct / len(y_test) * 100)
                #
                del model
        return np.round(score_sum / (k*5), 2)
